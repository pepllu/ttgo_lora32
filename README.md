```
> Importante: En el subdirectorio de biblioteca project_config hay un archivo lmic_project_config.h. En este archivo, debe elegir la entrada correcta para Europa:
> 
> // definiciones específicas del proyecto
>  #define CFG_eu868 1
> // # define CFG_us915 ​​1
> // # define CFG_au921 1
> // # define CFG_as923 1
> // #define LMIC_COUNTRY_CODE LMIC_COUNTRY_CODE_JP / * para as923-JP * /
> // # define CFG_in866 1
> #define CFG_sx1276_radio 1
> // # define LMIC_USE_INTERRUPTS
```



# IMPORTANT TO MAKE IT WORK
## LOOK FOR "SPI.begin();" in the library file hal.cpp and change it accorgint to your pings -> "SPI.begin(pin_SCK, pin_MISO, pin_MOSI, pin_SS)"" -> "SPI.begin(5,19,27,18);" that was causing an error 

# TTGO Lora32 V2

LoRa device based on the MCU ESP32, with Oled display and micro SD reader.

Install ESP32 Arduino Devkit: https://github.com/espressif/arduino-esp32

## Installation instructions using Arduino IDE Boards Manager [Install ESP32 for Arduino IDE](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md "Install ESP32 for Arduino IDE")

```
Enter https://dl.espressif.com/dl/package_esp32_index.json into Additional Board Manager URLs field

Install esp32 from Board Menu (**Heltec Wifi LoRa32**)
```


> Only for point to pont communitaction - Install library  "LoRa by Sandeep Mistry"

> The following library implements a LoRaWAN protocol stack that can be used with ESP32: LMIC-Arduino.
The LMIC-Arduino library can be found here: https://github.com/matthijskooijman/arduino-lmic

> A nice library for (on-board) (OLED) monochrome displays is: U8g2
The U8g2 library can be found here: https://github.com/olikraus/u8g2 47
(U8g2 includes U8x8 which is lower on resources.)
We need to choose U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);  on the code to get it working.


## PinOut
![PinOut](https://ptpimg.me/2xeysb.jpg?w=800&h=530&tok=d2f9f2 "TTGO Lora32 PinOut")

## Product Description：
* Working voltage: 1.8~3.7v
* Acceptable current：10~14mA
* Transmit current:120mA@+20dBm
* 90mA@+17dBm
* 29mA@+13dBm
* Operating frequency: 868M/915M
* Transmit power: +20dBm
* Receive sensitivity :-139dBm@LoRa &62.5Khz&SF=12&146bps
* -136dBm@LoRa &125Khz&SF=12&293bps
* -118dBm@LoRa &125Khz&SF=6&9380bps
* -123dBm@FSK&5Khz&1.2Kbps
* Frequency error：+/-15KHz
* FIFO space：64Byte
* Data rate：1.2K~300Kbps@FSK
* 0.018K~37.5Kbps@LoRa 
* Modulation Mode ：FSK,GFSK,MSK,GMSK,LoRa TM，OOK
* Interface form ： SPI
* Sleep current ：0.2uA@SLEEP
* 1.5uA@IDLE



Great site to learn about getting your TTGO Lora32 working on The Thinks Network. 
https://bricolabs.cc/wiki/guias/lora_ttn

Library LMIC for LoRa Node
https://github.com/mcci-catena/arduino-lmic